/*
 ============================================================================
 Name        : ProgDyn.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DAC.h"
#include "Glouton.h"
#include "pgcb.h"


	int main(void) {

		// Dichotomie
		int borninf = 0;
		int bornsup = 13;
		int array[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		int size_t = 13;
		int value = 5;
		int n = find_by_dichotomy(array, size_t, value);
		//printf("La réponse est : %d",n);
		assert(n + 1 == 6);

		// Glouton
		struct glout g = { { 60,100,120 }, { 10, 20, 30 }};

		    int val[] = { 60, 100, 120 };
		    int wt[] = { 10, 20, 30 };
		    int W = 50;
		    int nn = sizeof(val) / sizeof(val[0]);
		    /*printf("\n%d", Sacados(W, wt, val, nn));
		    printf("\n");*/
		    assert(Sacados(W, wt, val, nn) == 220);

		//PCBG algo
		    bool M[R][C] = {{0, 1, 1, 0, 1},
		                        {1, 1, 0, 1, 0},
		                        {0, 1, 1, 1, 0},
		                        {1, 1, 1, 1, 0},
		                        {1, 1, 1, 1, 1},
		                        {0, 0, 0, 0, 0}};

		        for (int i = 0; i < R; i++) {
		            for (int j = 0; j < C; j++) {
		                printf("%d ", M[i][j]);
		            }
		            printf("\n");
		        }
		        printMaxSquare(M);


		return EXIT_SUCCESS;
	}
