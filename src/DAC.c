/*
 * DAC.c
 *
 *  Created on: Oct 23, 2020
 *      Author: student
 */

#include <stdio.h>
#include <stdlib.h>


int ditcho(int array[], int value, int borninf, int bornsup) {
	int mid = (borninf + bornsup) / 2;
	if (array[mid] == value) {
		return mid;
	} else if (value < array[mid]) {
		return ditcho(array, value, 0, mid);
	} else {
		return ditcho(array, value, mid, bornsup);
	}
}

int find_by_dichotomy(int array[], int size_t, int value) {
	return ditcho(array, value, 0, size_t);
}
