/*
 * Glouton.h
 *
 *  Created on: Oct 23, 2020
 *      Author: student
 */

#ifndef GLOUTON_H_
#define GLOUTON_H_

int Sacados(int W, int wt[], int val[], int n);

int max(int a, int b);

struct glout {
	int wt;
	int val;
};

#endif /* GLOUTON_H_ */
