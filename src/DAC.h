/*
 * DAC.h
 *
 *  Created on: Oct 23, 2020
 *      Author: student
 */

#ifndef DAC_H_
#define DAC_H_

/**
 * Dichotomie
 * @param array créer un tableau de valeurs
 * @param size_t Taille du tableau
 * @param value La valeur a trouver
 * @return return function ditcho
 */

int find_by_dichotomy(int array[], int size_t, int value );

/**
 * test en dichotomie
 * @param array créer un tableau de valeurs
 * @param value La valeur a trouver
 * @param borninf Borne inférieur de l'intervalle
 * @param bornsup Borne supérieur de l'intervalle
 * @return The position of the value found or -1
 */

int ditcho(int array[], int value, int borninf, int bornsup);


#endif /* DAC_H_ */
